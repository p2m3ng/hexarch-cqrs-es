import hashlib
from uuid import UUID

from books.domain import commands
from books.domain.events import BookCreated, BookUpdated
from event_store.domain.aggregate import EventSourcedAggregate, method_dispatch
from event_store.domain.events import DomainEvent, EventStream
from event_store.services.mappers import aggregate_mapper


class Book(EventSourcedAggregate):
    def __init__(self, event_stream):
        super(Book, self).__init__(event_stream=event_stream)

    def __repr__(self):
        return f"<Book: {self.author} - {self.title}>"

    def _validate_types(self):
        assert isinstance(self.uuid, UUID)
        assert isinstance(self.author, str)
        assert isinstance(self.title, str)

    @staticmethod
    def _get_hash(author, title):
        chain = f"{author} :: {title}"
        return hashlib.sha1(chain.encode()).hexdigest()

    @classmethod
    def create(cls, cmd: commands.CreateBook):
        initial_event = BookCreated(
            author=cmd.author,
            title=cmd.title,
            hash=cls._get_hash(author=cmd.author, title=cmd.title),
        )
        instance = cls(event_stream=EventStream(events=[], version=0))
        instance.inject_event(event=initial_event)
        return instance

    def update(self, cmd: commands.UpdateBook):
        event = BookUpdated(
            author=cmd.author or self.author, title=cmd.title or self.title
        )
        self.inject_event(event=event)

    @method_dispatch
    def apply(self, event: DomainEvent):
        raise ValueError("Unknown event!")

    @apply.register(BookCreated)
    def _(self, event: BookCreated):
        for parameter, value in event.data.items():
            setattr(self, parameter, value)

    @apply.register(BookUpdated)
    def _(self, event: BookUpdated):
        for parameter, value in event.data.items():
            setattr(self, parameter, value)


aggregate_mapper.register(Book)
