import abc

from books.exceptions import BooksManagementError


class BookRepository(abc.ABC):  # pragma: no cover
    def add(self, book):
        if self._get_by_hash(book.hash):
            raise BooksManagementError("Existing reference.")
        self._add(book)

    def get(self, book_id):
        book = self._get(book_id=book_id)
        if not book:
            raise BooksManagementError("This reference does not exist.")
        return book

    def update(self, book):
        raise NotImplementedError

    @abc.abstractmethod  # pragma: no mutate
    def all(self):
        raise NotImplementedError

    @abc.abstractmethod  # pragma: no mutate
    def _add(self, book):
        raise NotImplementedError

    @abc.abstractmethod
    def _get(self, book_id):
        raise NotImplementedError

    @abc.abstractmethod
    def _get_by_hash(self, hash):
        raise NotImplementedError


class InMemoryBookRepository(BookRepository):
    def __init__(self):
        self._books = set()

    def _add(self, book):
        return self._books.add(book)

    def _get(self, book_id):
        return next((p for p in self._books if p.uuid == book_id), None)

    def _get_by_hash(self, hash):
        return next((p for p in self._books if p.hash == hash), None)

    def update(self, book):
        pass

    def all(self):
        return list(self._books)
