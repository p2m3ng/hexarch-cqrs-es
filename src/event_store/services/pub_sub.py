import abc

from event_store.domain.events import DomainEvent


class EventsListener(abc.ABC):  # pragma: no cover
    @abc.abstractmethod
    def warn(self, event):
        raise NotImplementedError


class EventManager:
    def __init__(self):
        self.listeners = []

    def subscribe(self, event: DomainEvent, listener: EventsListener):
        subscription = {"event": event, "listener": listener}
        self.listeners.append(subscription)
        print(f"New subscriber: {listener} to event {event.__name__}")

    def notify(self, event: DomainEvent):
        for listener in self.listeners:
            self.listeners
            if listener["event"] == event.__class__:
                listener["listener"].warn(event)
