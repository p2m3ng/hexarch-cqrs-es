from books.domain import events
from event_store.services.mappers import aggregate_mapper
from .notifications import EmailAlertsListener

email_alerts_listener = EmailAlertsListener(
    name="Created books subscription", recipients=["admin@example.com"]
)

book = aggregate_mapper.get("Book")

book.events.subscribe(event=events.BookCreated, listener=email_alerts_listener)
