from click.testing import CliRunner

from books.adapters.primaries import cli
from settings import get_config
from uuid import UUID


class TestCLI:
    def test_should_return_add_book_options(self):
        runner = CliRunner()
        result = runner.invoke(cli.cli, ["books", "add", "--help"])
        assert "-a, --author TEXT  Book author" in result.output
        assert "-t, --title TEXT   Book title" in result.output

    def test_should_create_book(self):
        runner = CliRunner()
        result = runner.invoke(
            cli.cli, ["books", "add", "-a", "John Doe", "-t", "Thoughts"]
        )
        assert result.exit_code == 0
        assert "<Book: John Doe - Thoughts>" in result.output

    def test_should_list_books(self):
        runner = CliRunner()
        runner.invoke(cli.cli, ["books", "add", "-a", "John Doe", "-t", "Thoughts"])
        result = runner.invoke(cli.cli, ["books", "list"])
        assert (
            "3ef816ad5a2cc15ce0e9df567a3a3c8512118e3a: John Doe - Thoughts"
            in result.output
        )

    def test_list_book_should_return_empty_bookcase(self):
        config = get_config()
        config.uow.books._books = set()
        runner = CliRunner()
        result = runner.invoke(cli.cli, ["books", "list"])
        assert "The bookcase is empty!\n" == result.output

    def test_should_update_book(self):
        config = get_config()
        config.uow.books._books = set()
        runner = CliRunner()
        book = runner.invoke(
            cli.cli, ["books", "add", "-a", "John Doe", "-t", "Thoughts"]
        )
        for line in book.output.split("\n"):
            try:
                uuid = str(UUID(line.rstrip()))
            except Exception:
                pass
        result = runner.invoke(
            cli.cli,
            [
                "books",
                "update",
                "--uuid",
                uuid,
                "-a",
                "john doe",
                "-t",
                "Toto est têtu",
            ],
        )
        assert "<Book: john doe - Toto est têtu>" in result.output

    def test_should_return_update_book_options(self):
        runner = CliRunner()
        result = runner.invoke(cli.cli, ["books", "update", "--help"])
        assert "-u, --uuid TEXT    Book uuid" in result.output
        assert "-a, --author TEXT  Book author" in result.output
        assert "-t, --title TEXT   Book title" in result.output
