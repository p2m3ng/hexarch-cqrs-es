def pre_mutation(context):
    line = context.current_source_line.strip()
    if line.startswith('@dataclass'):
        context.skip = True
    elif line.startswith('@abc.abstractmethod'):
        context.skip = True
    elif line.startswith('@staticmethod'):
        context.skip = True
