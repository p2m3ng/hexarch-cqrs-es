import abc
from dataclasses import dataclass
from uuid import UUID
from typing import Optional


class Command(abc.ABC):  # pragma: no cover
    pass


@dataclass
class CreateBook(Command):
    author: str
    title: str


@dataclass
class UpdateBook(Command):
    uuid: UUID
    author: Optional[str] = None
    title: Optional[str] = None
