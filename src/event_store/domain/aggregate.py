import abc
import functools
from dataclasses import asdict

from event_store.domain.events import DomainEvent, EventStream
from event_store.services.pub_sub import EventManager


def method_dispatch(func):
    """Allows the decorated method to have different behaviours depending on
    the first parameter type.
    """
    dispatcher = functools.singledispatch(func)

    def wrapper(*args, **kwargs):
        return dispatcher.dispatch(args[1].__class__)(*args, **kwargs)

    wrapper.register = dispatcher.register
    functools.update_wrapper(wrapper, func)
    return wrapper


class EventSourcedAggregate(abc.ABC):  # pragma: no cover
    events = EventManager()

    def __init__(self, event_stream: EventStream):
        self.event_stream = event_stream
        self.version: int = event_stream.version

        for event in event_stream.events:
            self.apply(event)

        self.changes = []

    def inject_event(self, event: DomainEvent):
        event.data = asdict(event)
        self.changes.append(event)
        self.event_stream.events.append(event)
        self.apply(event)
        self._validate_types()
        self.events.notify(event=event)

    @abc.abstractmethod
    def _validate_types(self):
        raise NotImplementedError

    @method_dispatch
    def apply(self, event: DomainEvent):
        raise NotImplementedError
