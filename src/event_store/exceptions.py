class AggregateRootNotProvided(Exception):
    pass


class InvalidEventError(Exception):
    pass


class InvalidAggregateError(Exception):
    pass
