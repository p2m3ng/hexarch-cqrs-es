from uuid import UUID

import pytest

from books.domain import commands
from books.domain.book import Book
from books.domain.usecases import add_book, update_book
from books.exceptions import BooksManagementError
from books.services.unit_of_work import InMemoryUnitOfWork
from tests.conftest import FakeEvent
from event_store.domain.events import EventStream


class TestAggregate:
    def test_should_fail_on_applying_wrong_event(self):
        stream = EventStream([FakeEvent()], version=1)
        with pytest.raises(ValueError) as error:
            Book(event_stream=stream)
        assert str(error.value) == "Unknown event!"


class TestBookModel:
    def test_book_model(self, create_book_cmd):
        book = Book.create(create_book_cmd)
        assert book.version == 0
        assert book.event_stream.events[0].uuid == book.uuid
        assert isinstance(book.uuid, UUID)
        assert book.hash == "3ef816ad5a2cc15ce0e9df567a3a3c8512118e3a"
        assert book.author == "John Doe"
        assert book.title == "Thoughts"
        assert repr(book) == "<Book: John Doe - Thoughts>"


class TestBooksQueries:
    def test_get_collection_of_books(self):
        uow = InMemoryUnitOfWork()
        book1 = commands.CreateBook(
            author="Emmanuel Kant", title="Critique de la raison pure"
        )
        book2 = commands.CreateBook(
            author="Emmanuel Kant", title="Critique de la raison pratique"
        )
        add_book(cmd=book1, uow=uow)
        add_book(cmd=book2, uow=uow)
        assert len(uow.books.all()) == 2

    def test_get_one_book(self, book_fixture):
        cmd, uow = commands.CreateBook(**book_fixture), InMemoryUnitOfWork()
        assert uow.committed is False
        book_id = add_book(cmd=cmd, uow=uow)
        book = uow.books.get(book_id=book_id)
        assert uow.committed
        assert isinstance(book.uuid, UUID)
        assert book.author == "John Doe"
        assert book.title == "Thoughts"

    def test_error_for_invalid_book_id(self):
        uow = InMemoryUnitOfWork()
        with pytest.raises(BooksManagementError) as error:
            uow.books.get(book_id="abc123")
        assert "This reference does not exist." == str(error.value)


class TestBookUseCases:
    def test_create_book(self, create_book_cmd):
        uow = InMemoryUnitOfWork()
        book_id = add_book(cmd=create_book_cmd, uow=uow)
        assert uow.committed
        assert uow.event_store.stored_events is not None
        assert uow.books.get(book_id=book_id) is not None

    def test_error_create_existing_book(self, book_fixture):
        cmd, uow = commands.CreateBook(**book_fixture), InMemoryUnitOfWork()
        with pytest.raises(BooksManagementError) as error:
            add_book(cmd=cmd, uow=uow)
            add_book(cmd=cmd, uow=uow)
        assert "Existing reference." == str(error.value)

    def test_update_book(self, create_book_cmd):
        uow = InMemoryUnitOfWork()
        book_id = add_book(cmd=create_book_cmd, uow=uow)

        cmd = commands.UpdateBook(uuid=book_id, title="What Is Your Quest?")
        update_book(cmd, uow)
        book = uow.books.get(book_id=book_id)
        assert book.title == "What Is Your Quest?"
