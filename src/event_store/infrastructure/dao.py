from dataclasses import dataclass
from datetime import datetime
from uuid import UUID


@dataclass
class EventStoreDao:
    uuid: UUID  # Event identifier
    created_at: datetime  # Timestamp
    # sequence: int  # Order of occurred events
    version: int  # Used in optimistic concurrency check
    name: str  # Event name
    aggregate_id: UUID  # ID of the given aggregate
    data: str  # Event payload, serialized as JSON string
    aggregate: str  # Aggregate name
