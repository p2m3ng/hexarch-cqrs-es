import pytest

from event_store.domain.events import DomainEvent
from event_store.domain.aggregate import EventSourcedAggregate
from books.domain import commands


class FakeEvent(DomainEvent):
    pass


class FakeAggregate(EventSourcedAggregate):
    def _validate_types(self):
        pass

    def apply(self, event: EventSourcedAggregate):
        pass


@pytest.fixture
def book_fixture():
    return {"author": "John Doe", "title": "Thoughts"}


@pytest.fixture
def create_book_cmd():
    return commands.CreateBook(author="John Doe", title="Thoughts")
