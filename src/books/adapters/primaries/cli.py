import click
from books.domain import usecases
from books.domain import commands
from settings import get_config
from uuid import UUID


config = get_config()


@click.group()
def cli():
    """Bookstore CLI management"""


@cli.group("books")
def books():
    pass


@books.command("add")
@click.option("--author", "-a", help="Book author")
@click.option("--title", "-t", help="Book title")
def add_book(author, title):
    """Add book to bookcase"""
    uow = config.uow
    cmd = commands.CreateBook(author=author, title=title)
    book_id = usecases.add_book(cmd=cmd, uow=uow)
    book = uow.books.get(book_id=book_id)
    click.echo(
        click.style(f"New book created\n{book.uuid}\n\t{repr(book)}", fg="green")
    )


@books.command("list")
def list_books():
    """List bookcase content"""
    uow = config.uow
    bookcase = uow.books.all()
    if not bookcase:
        click.echo(click.style("The bookcase is empty!", fg="red"))
        return
    for book in bookcase:
        click.echo(f"{book.hash}: {book.author} - {book.title}")


@books.command("update")
@click.option("--uuid", "-u", help="Book uuid")
@click.option("--author", "-a", help="Book author")
@click.option("--title", "-t", help="Book title")
def update_book(uuid, author, title):
    """Update book content"""
    uow = config.uow
    uuid = UUID(uuid)
    cmd = commands.UpdateBook(uuid=uuid, author=author, title=title)
    usecases.update_book(cmd=cmd, uow=uow)
    click.echo(uow.books.get(uuid))
