import abc

from event_store.exceptions import InvalidAggregateError, InvalidEventError
from event_store.domain.events import DomainEvent
from event_store.domain.aggregate import EventSourcedAggregate


class Mapper(abc.ABC):  # pragma: no cover
    @abc.abstractmethod
    def register(self, *args):
        raise NotImplementedError

    @abc.abstractmethod
    def get(self, *args):
        raise NotImplementedError


class AggregateMapper(Mapper):
    def __init__(self):
        self.aggregates = {}

    def register(self, aggregate: type) -> None:
        if not issubclass(aggregate, EventSourcedAggregate):
            raise InvalidAggregateError(
                f"Registered object should be an aggregate, got {type(aggregate)}."
            )
        self.aggregates[aggregate.__name__] = aggregate

    def get(self, aggregate_name) -> EventSourcedAggregate:
        try:
            return self.aggregates[aggregate_name]
        except KeyError:
            raise InvalidEventError(f"Unregistered aggregate: {aggregate_name}")


class EventsMapper(Mapper):
    def __init__(self):
        self.events = {}

    def register(self, event: type):
        if not issubclass(event, DomainEvent):
            raise InvalidEventError(
                f"Registered object should be an Event, got {type(event)}."
            )
        self.events[event.__name__] = event

    def get(self, event_name):
        try:
            return self.events[event_name]
        except KeyError:
            raise InvalidEventError(f"Unregistered event: {event_name}")


aggregate_mapper = AggregateMapper()
events_mapper = EventsMapper()
